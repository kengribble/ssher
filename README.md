# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* perl program called ssher

### How do I get set up? ###

* requires perl
* download and ensure the #!/usr/local/perl is set to the correct directory for your system

### Useage ###

$ ssher [-v] -l listname *command* 

-v is verbose output 

-l is a file name with a required list of IP addresses or hostnames in that file

*command* is the required command to send to the list of nodes with ssh