#!/usr/bin/env python3

import argparse
import subprocess
import os
import sys
import socket
from typing import List

def is_host_up(host: str, port: int = 22, timeout: float = 2) -> bool:
    try:
        with socket.create_connection((host, port), timeout=timeout):
            return True
    except (socket.error, socket.timeout):
        return False

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    parser.add_argument("-u", "--user", help="User to submit commands")
    parser.add_argument("-k", "--keyless", action="store_true", help="Keyless (password required)")
    parser.add_argument("-f", "--file", required=True, help="File name with a list of IP addresses or hostnames")
    parser.add_argument("command", nargs="+", help="Command to send to the file of nodes with ssh")
    args = parser.parse_args()

    verbose = args.verbose
    user = args.user
    keyless = args.keyless
    file = args.file
    command = " ".join(args.command)

    options = ["-o", "ConnectTimeout=10"]
    if keyless:
        options.extend(["-o", "PreferredAuthentications=password", "-o", "PubkeyAuthentication=no"])
        if verbose:
            print(f"Keyless mode (keyless = {keyless}) adding options({options})")

    if user:
        options.extend(["-l", user])

    with open(file, "r") as f:
        nodes = [line.strip() for line in f.readlines() if not line.strip().startswith("#")]

    if verbose:
        print(f"Nodes: {nodes}")
        print(f"Command = {command}")

    for node in nodes:
        if verbose:
            print(f"ssh {' '.join(options)} {node} {command}")

        if is_host_up(node):
            print(f"{node}:")
            if verbose:
                print(f"Issuing: ssh {' '.join(options)} {node} {command}")

            subprocess.run(["ssh", *options, node, command])
        else:
            print(f"{node}: DOWN")

        print()

if __name__ == "__main__":
    main()

